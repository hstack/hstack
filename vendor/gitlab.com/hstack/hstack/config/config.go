package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/hashicorp/go-multierror"
	"github.com/hashicorp/hcl"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/hstack/hstack/config/atlas"
	"gitlab.com/hstack/hstack/config/aws"
	"gitlab.com/hstack/hstack/config/consul"
	"gitlab.com/hstack/hstack/config/nomad"
	"gitlab.com/hstack/hstack/config/openvpn"
	"gitlab.com/hstack/hstack/config/ssh"
	"gitlab.com/hstack/hstack/config/terraform"
	"gitlab.com/hstack/hstack/config/vault"
	"gitlab.com/hstack/hstack/utils"
	"strings"
)

// defaultAdminUsers is the default list of users that will be provisionned
// in the hstack admin group, separated by comma
const defaultAdminUsers = "hstack-admin"

// Config is used to configure hss projets outputs
type Config struct {
	// Path is the path to this configuration file on disk. This value is not
	// read from disk by rather dynamically populated by the code so the Config
	// has a reference to the path to the file on disk that created it.
	Path string `mapstructure:"-"`

	// Version is the version of hstack used. This value is set dynamically
	Version string `mapstructure:"-"`

	// Name is the name of the project
	Name string `mapstructure:"name"`

	// global_tags are tags that will be set to any resource
	// supporting tags.
	GlobalTags []string `mapstructure:"global_tags"`

	// admin_users are the usernames of the users that will be
	// provisionned and added to the admin hstack group
	AdminUsers []string `mapstructure:"admin_users"`

	// Openvpn is the openvpn configuration.
	Openvpn *openvpn.Config `mapstructure:"openvpn"`

	// Atlas is the atlas configuration.
	Atlas *atlas.Config `mapstructure:"atlas"`

	// Consul is the consul configuration.
	Consul *consul.Config `mapstructure:"consul"`

	// Terraform is the terraform configuration.
	Terraform *terraform.Config `mapstructure:"terraform"`

	// Vault is the vault configuration.
	Vault *vault.Config `mapstructure:"vault"`

	// Nomad is the nomad configuration.
	Nomad *nomad.Config `mapstructure:"nomad"`

	// Ssh is the ssh configuration used by the terraform project
	// to connect to servers and populate authorized_keys
	Ssh *ssh.Config `mapstructure:"ssh"`

	// Provider is dynamically built and set from the ProviderOptions
	Provider ProviderConfig `mapstructure:"-"`

	// ProviderOptions is the config used by the provider such as aws regions,
	// subnets, az, instance types according to the terraform variables each provider
	// can accept
	// Once validated it will be used to set the Provider config.
	ProviderOptions map[string]interface{} `mapstructure:"provider"`

	// setKeys is the list of config keys that were set by the user.
	setKeys map[string]struct{}
}

func (c *Config) String() string {
	return fmt.Sprintf(`
	Path: %s,
	Version: %s,
	Name: %s,
	GlobalTags: %v,
	AdminUsers: %v,
	Openvpn: %v,
	Atlas: %v,
	Consul: %v,
	Terraform: %v,
	Vault: %v,
	Nomad: %v,
        Ssh: %v,
        Provider: %v,
`,
		c.Path,
		c.Version,
		c.Name,
		c.GlobalTags,
		c.AdminUsers,
		c.Openvpn,
		c.Atlas,
		c.Consul,
		c.Terraform,
		c.Vault,
		c.Nomad,
		c.Ssh,
		c.Provider,
	)
}

// Copy returns a deep copy of the current configuration. This is useful because
// the nested data structures may be shared.
func (c *Config) Copy() *Config {
	config := new(Config)
	config.Path = c.Path
	config.Name = c.Name
	config.Version = c.Version

	config.GlobalTags = make([]string, len(c.GlobalTags))
	copy(c.GlobalTags, config.GlobalTags)
	config.AdminUsers = make([]string, len(c.AdminUsers))
	copy(c.AdminUsers, config.AdminUsers)

	if c.Openvpn != nil {
		config.Openvpn = c.Openvpn.Copy()
	}

	if c.Atlas != nil {
		config.Atlas = c.Atlas.Copy()
	}

	if c.Consul != nil {
		config.Consul = c.Consul.Copy()
	}

	if c.Terraform != nil {
		config.Terraform = c.Terraform.Copy()
	}

	if c.Vault != nil {
		config.Vault = c.Vault.Copy()
	}

	if c.Nomad != nil {
		config.Nomad = c.Nomad.Copy()
	}

	if c.Ssh != nil {
		config.Ssh = c.Ssh.Copy()
	}

	if c.ProviderOptions != nil {
		config.ProviderOptions = make(map[string]interface{})
		for k, v := range c.ProviderOptions {
			config.ProviderOptions[k] = v
		}
	}

	config.setKeys = c.setKeys
	return config
}

// Merge merges the values in config into this config object. Values in the
// config object overwrite the values in c.
func (c *Config) Merge(config *Config) {
	if config.WasSet("name") {
		c.Name = config.Name
	}

	if config.WasSet("version") {
		c.Version = config.Version
	}

	if config.WasSet("global_tags") {
		c.GlobalTags = config.GlobalTags
	}

	if config.WasSet("admin_users") {
		c.AdminUsers = config.AdminUsers
	}

	if config.WasSet("path") {
		c.Path = config.Path
	}

	if config.WasSet("openvpn") {
		if c.Openvpn == nil {
			c.Openvpn = &openvpn.Config{}
		}
		c.Openvpn.Merge(config.Openvpn, config.setKeys)
	}

	if config.WasSet("atlas") {
		if c.Atlas == nil {
			c.Atlas = &atlas.Config{}
		}

		c.Atlas.Merge(config.Atlas, config.setKeys)
	}

	if config.WasSet("consul") {
		if c.Consul == nil {
			c.Consul = &consul.Config{}
		}
		c.Consul.Merge(config.Consul, config.setKeys)
	}

	if config.WasSet("terraform") {
		if c.Terraform == nil {
			c.Terraform = &terraform.Config{}
		}
		c.Terraform.Merge(config.Terraform, config.setKeys)
	}

	if config.WasSet("vault") {
		if c.Vault == nil {
			c.Vault = &vault.Config{}
		}
		c.Vault.Merge(config.Vault, config.setKeys)
	}

	if config.WasSet("nomad") {
		if c.Nomad == nil {
			c.Nomad = &nomad.Config{}
		}
		c.Nomad.Merge(config.Nomad, config.setKeys)
	}

	if config.WasSet("ssh") {
		if c.Ssh == nil {
			c.Ssh = &ssh.Config{}
		}
		c.Ssh.Merge(config.Ssh, config.setKeys)
	}

	if config.WasSet("provider") {
		if c.ProviderOptions == nil {
			c.ProviderOptions = make(map[string]interface{})
		}
		for k, v := range config.ProviderOptions {
			c.ProviderOptions[k] = v
		}
	}

	if c.setKeys == nil {
		c.setKeys = make(map[string]struct{})
	}
	for k := range config.setKeys {
		if _, ok := c.setKeys[k]; !ok {
			c.setKeys[k] = struct{}{}
		}
	}
}

// WasSet determines if the given key was set in the config (as opposed to just
// having the default value).
func (c *Config) WasSet(key string) bool {
	if _, ok := c.setKeys[key]; ok {
		return true
	}
	return false
}

// set is a helper function for marking a key as set.
func (c *Config) Set(key string) {
	if _, ok := c.setKeys[key]; !ok {
		c.setKeys[key] = struct{}{}
	}
}

func (c *Config) Validate() error {
	var errs *multierror.Error

	if c.Name == "" {
		errs = multierror.Append(errs, fmt.Errorf("name is required"))

	}

	if c.Version == "" {
		errs = multierror.Append(errs, fmt.Errorf("version is required"))
	}

	if c.ProviderOptions == nil {
		errs = multierror.Append(errs, fmt.Errorf("Provider is required"))
	}

	switch providerType := c.ProviderOptions["type"]; providerType {
	case "aws":
		awsConfig, err := aws.ParseConfig(c.ProviderOptions)
		if err != nil {
			errs = multierror.Append(errs, err)
			return errs.ErrorOrNil()
		}

		if err = awsConfig.Validate(); err != nil {
			errs = multierror.Append(errs, fmt.Errorf("provider config errors: %v", err))
		} else {
			c.Provider = awsConfig
		}
	case "ovh":
	case "vbox":
	default:
		errs = multierror.Append(errs, fmt.Errorf("unknown provider type %s", providerType))
	}

	if err := c.Consul.Validate(); err != nil {
		errs = multierror.Append(errs, err)
	}
	if err := c.Atlas.Validate(); err != nil {
		errs = multierror.Append(errs, err)
	}
	if err := c.Vault.Validate(); err != nil {
		errs = multierror.Append(errs, err)
	}
	if err := c.Terraform.Validate(); err != nil {
		errs = multierror.Append(errs, err)
	}
	if err := c.Nomad.Validate(); err != nil {
		errs = multierror.Append(errs, err)
	}
	if err := c.Ssh.Validate(); err != nil {
		errs = multierror.Append(errs, err)
	}
	if err := c.Openvpn.Validate(); err != nil {
		errs = multierror.Append(errs, err)
	}

	if c.Ssh.Keypairs[c.Terraform.SshKeypair] == nil {
		errs = multierror.Append(errs, fmt.Errorf("You must define a ssh.keypair for terraform with key %q", c.Terraform.SshKeypair))
	}

	found := false
	for _, k := range c.Ssh.AuthorizedKeys {
		if k == c.Terraform.SshKeypair {
			found = true
		}
	}
	if !found {
		errs = multierror.Append(errs, fmt.Errorf("Terraform keypair %q must be declared in ssh.authorized_keys", c.Terraform.SshKeypair))

	}

	return errs.ErrorOrNil()
}

// ParseConfig reads the configuration file at the given path and returns a new
// Config struct with the data populated.
func ParseConfig(path string) (*Config, error) {
	var errs *multierror.Error

	// Read the contents of the file
	contents, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("error reading config at %q: %s", path, err)
	}

	// Parse the file (could be HCL or JSON)
	var shadow interface{}
	if err := hcl.Decode(&shadow, string(contents)); err != nil {
		return nil, fmt.Errorf("error decoding config at %q: %s", path, err)
	}

	// Convert to a map and flatten the keys we want to flatten
	parsed, ok := shadow.(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("error converting config at %q", path)
	}
	utils.FlattenKeys(parsed)

	// Create a new, empty config
	config := new(Config)

	// Use mapstructure to populate the basic config fields
	metadata := new(mapstructure.Metadata)
	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		DecodeHook: mapstructure.ComposeDecodeHookFunc(
			mapstructure.StringToSliceHookFunc(","),
			mapstructure.StringToTimeDurationHookFunc(),
		),
		ErrorUnused: true,
		Metadata:    metadata,
		Result:      config,
	})

	if err != nil {
		errs = multierror.Append(errs, err)
		return nil, errs.ErrorOrNil()
	}

	if err := decoder.Decode(parsed); err != nil {
		errs = multierror.Append(errs, err)
		return nil, errs.ErrorOrNil()
	}

	// Store a reference to the path where this config was read from
	config.Path = path

	// Update the list of set keys
	if config.setKeys == nil {
		config.setKeys = make(map[string]struct{})
	}

	for _, key := range metadata.Keys {
		if _, ok := config.setKeys[key]; !ok {
			config.setKeys[key] = struct{}{}
		}
	}
	config.setKeys["path"] = struct{}{}

	d := DefaultConfig()
	d.Merge(config)
	config = d

	return config, errs.ErrorOrNil()
}

// ConfigFromPath iterates and merges all configuration files in a given
// directory, returning the resulting config.
func ConfigFromPath(path string) (*Config, error) {
	// Ensure the given filepath exists
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return nil, fmt.Errorf("config: missing file/folder: %s", path)
	}

	// Check if a file was given or a path to a directory
	stat, err := os.Stat(path)
	if err != nil {
		return nil, fmt.Errorf("config: error stating file: %s", err)
	}

	// Recursively parse directories, single load files
	if stat.Mode().IsDir() {
		// Ensure the given filepath has at least one config file
		_, err := ioutil.ReadDir(path)
		if err != nil {
			return nil, fmt.Errorf("config: error listing directory: %s", err)
		}

		// Create a blank config to merge off of
		config := DefaultConfig()

		// Potential bug: Walk does not follow symlinks!
		err = filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
			// If WalkFunc had an error, just return it
			if err != nil {
				return err
			}

			// Do nothing for directories
			if info.IsDir() {
				return nil
			}

			// Parse and merge the config
			newConfig, err := ParseConfig(path)
			if err != nil {
				return err
			}
			config.Merge(newConfig)

			return nil
		})

		if err != nil {
			return nil, fmt.Errorf("config: walk error: %s", err)
		}

		return config, nil
	} else if stat.Mode().IsRegular() {
		return ParseConfig(path)
	}

	return nil, fmt.Errorf("config: unknown filetype: %q", stat.Mode().String())
}

// DefaultConfig returns the default configuration struct.
func DefaultConfig() *Config {
	config := &Config{
		AdminUsers: strings.Split(defaultAdminUsers, ","),
		Openvpn:    openvpn.DefaultConfig(),
		Consul:     consul.DefaultConfig(),
		Vault:      vault.DefaultConfig(),
		Nomad:      nomad.DefaultConfig(),
		Atlas:      atlas.DefaultConfig(),
		Terraform:  terraform.DefaultConfig(),
		Ssh:        ssh.DefaultConfig(),
		setKeys:    make(map[string]struct{}),
	}

	return config
}

// if a.InfluxDBUrl != "" {
// 	if _, err := url.Parse(a.InfluxDBUrl); err != nil {
// 		return fmt.Errorf("influxdb-url must be a valid url")
// 	}

// 	if a.InfluxDBName == "" {
// 		log.Printf("[DEBUG] Influxdb db name is not set, defaulting to %s.%s\n", a.Name, a.Domain)
// 		a.InfluxDBName = fmt.Sprintf("%s.%s", a.Name, a.Domain)
// 	}
// 	if a.InfluxDBUser == "" {
// 		log.Printf("[DEBUG] Influxdb db user is not set, defaulting to telegraf\n")
// 		a.InfluxDBName = "telegraf"
// 	}
// 	if a.InfluxDBPassword == "" {
// 		log.Printf("[DEBUG] Influxdb db password is not set, defaulting to telegraf\n")
// 		a.InfluxDBPassword = "telegraf"
// 	}
// }

// if a.GelfEndpoint != "" {
// 	sink := strings.Split(a.GelfEndpoint, ":")

// 	if len(sink) != 2 || net.ParseIP(sink[0]) == nil {
// 		return fmt.Errorf("graylog-endpoint must be formatted as an IP:PORT")
// 	}

// 	if _, err := strconv.Atoi(sink[1]); err != nil {
// 		return fmt.Errorf("graylog-endpoint must be formatted as an IP:PORT")
// 	}
// }
