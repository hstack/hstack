package atlas

import (
	"fmt"

	"github.com/hashicorp/go-multierror"
)

// defaultUsername is the default username used to retrieve info from atlas
const defaultUsername = "hstack"

// Copy returns a deep copy of the current configuration. This is useful because
// the nested data structures may be shared.
func (c *Config) Copy() *Config {
	return &Config{
		Username: c.Username,
	}
}

func (c *Config) Merge(config *Config, setKeys map[string]struct{}) {
	if WasSet(setKeys, "atlas.username") {
		c.Username = config.Username
	}
}

// WasSet determines if the given key was set in the config (as opposed to just
// having the default value).
func WasSet(setKeys map[string]struct{}, key string) bool {
	if _, ok := setKeys[key]; ok {
		return true
	}
	return false
}

// Atlas is the configuration for atlas
type Config struct {
	// username - is the username
	Username string `mapstructure:"username"`
}

// String is the string representation of this atlas configuration.
func (c *Config) String() string {
	return fmt.Sprintf("username: %s", c.Username)
}
func (c *Config) Validate() error {
	var errs *multierror.Error
	if c.Username == "" {
		errs = multierror.Append(errs, fmt.Errorf("atlas.username is required"))
	}
	return errs.ErrorOrNil()
}

func DefaultConfig() *Config {
	config := &Config{
		Username: defaultUsername,
	}
	return config
}
