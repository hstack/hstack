package terraform

import (
	"fmt"
	"github.com/hashicorp/go-multierror"
)

// defaultEnableRemoteState is the default value used to
// enable a terraform remote state backend.
const defaultEnableRemoteState = false

// defaultSshKeypair is the default ssh private key path terraform
// will use for remote provionning
const defaultSshKeypair = "terraform"

// Copy returns a deep copy of the current configuration. This is useful because
// the nested data structures may be shared.
func (c *Config) Copy() *Config {
	return &Config{
		EnableRemoteState: c.EnableRemoteState,
		SshKeypair:        c.SshKeypair,
	}
}

func (c *Config) Merge(config *Config, setKeys map[string]struct{}) {
	if WasSet(setKeys, "terraform.enable_remote_state") {
		c.EnableRemoteState = config.EnableRemoteState
	}
	if WasSet(setKeys, "terraform.ssh_keypair") {
		c.SshKeypair = config.SshKeypair
	}
}

// WasSet determines if the given key was set in the config (as opposed to just
// having the default value).
func WasSet(setKeys map[string]struct{}, key string) bool {
	if _, ok := setKeys[key]; ok {
		return true
	}
	return false
}

// Atlas is the configuration for atlas
type Config struct {
	// enable_remote_state - is the prodiver which will be used to configure
	// terraform remote state
	EnableRemoteState bool `mapstructure:"enable_remote_state"`

	// ssh_keypair - is the path to the ssh private key terraform will use
	// for remote provisionning
	SshKeypair string `mapstructure:"ssh_keypair"`
}

// String is the string representation of this atlas configuration.
func (c *Config) String() string {
	return fmt.Sprintf("enable_remote_state: %v, ssh_keypair: %s", c.EnableRemoteState, c.SshKeypair)
}

func (c *Config) Validate() error {
	var errs *multierror.Error

	return errs.ErrorOrNil()
}

func DefaultConfig() *Config {
	config := &Config{
		EnableRemoteState: defaultEnableRemoteState,
		SshKeypair:        defaultSshKeypair,
	}
	return config
}
