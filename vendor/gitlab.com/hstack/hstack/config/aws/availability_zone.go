package aws

import (
	"fmt"

	"github.com/hashicorp/go-multierror"
)

// Copy returns a deep copy of the current configuration. This is useful because
// the nested data structures may be shared.
func (c *AvailabilityZoneConfig) Copy() *AvailabilityZoneConfig {
	return &AvailabilityZoneConfig{}
}

// Atlas is the configuration for atlas
type AvailabilityZoneConfig struct {
}

// String is the string representation of this atlas configuration.
func (c *AvailabilityZoneConfig) String() string {
	return fmt.Sprintf("{}")
}

func (c *AvailabilityZoneConfig) Validate() error {
	var errs *multierror.Error

	return errs.ErrorOrNil()
}
