package consul

import (
	"fmt"
	"github.com/hashicorp/go-multierror"
	"net"
)

// defaultDomain is the default domain set for consul
const defaultDomain = "consul"

// defaultDC is the default dc set for consul
const defaultDC = "dc1"

// defaultServersCount is the default number of consul servers to bootstrap
const defaultServersCount = 3

// Copy returns a deep copy of the current configuration. This is useful because
// the nested data structures may be shared.
func (c *Config) Copy() *Config {
	return &Config{
		ServersCount: c.ServersCount,
		Domain:       c.Domain,
		DC:           c.DC,
		JoinIP:       c.JoinIP,
		JoinIPWan:    c.JoinIPWan,
	}
}

func (c *Config) Merge(config *Config, setKeys map[string]struct{}) {
	if WasSet(setKeys, "consul.servers_count") {
		c.ServersCount = config.ServersCount
	}
	if WasSet(setKeys, "consul.domain") {
		c.Domain = config.Domain
	}
	if WasSet(setKeys, "consul.dc") {
		c.DC = config.DC
	}
	if WasSet(setKeys, "consul.joinip") {
		c.JoinIP = config.JoinIP
	}
	if WasSet(setKeys, "consul.joinip_wan") {
		c.JoinIPWan = config.JoinIPWan
	}
}

// WasSet determines if the given key was set in the config (as opposed to just
// having the default value).
func WasSet(setKeys map[string]struct{}, key string) bool {
	if _, ok := setKeys[key]; ok {
		return true
	}
	return false
}

// Atlas is the configuration for atlas
type Config struct {
	// servers_count - number of consul servers
	ServersCount int `mapstructure:"servers_count"`

	// domain : consul domain
	// required
	Domain string `mapstructure:"domain"`

	// dc: consul dc
	// required, defaults to dc1
	DC string `mapstructure:"dc"`

	// joinip - The ip of a consul server to join.
	// It can be useful to merge clusters.
	JoinIP string `mapstructure:"joinip"`

	// joinip_wan - The ip of a consul server to join on wan.
	// It can be useful to merge clusters from 2 datacenters.
	// Note : this option will only be applied to server nodes.
	JoinIPWan string `mapstructure:"joinip_wan"`
}

// String is the string representation of this atlas configuration.
func (c *Config) String() string {
	return fmt.Sprintf(
		"{ servers_count: %d, domain: %s, dc: %s, joinip: %s, joinip_wan: %s }",
		c.ServersCount,
		c.Domain,
		c.DC,
		c.JoinIP,
		c.JoinIPWan,
	)
}
func (c *Config) Validate() error {
	var errs *multierror.Error

	if c.Domain == "" {
		errs = multierror.Append(errs, fmt.Errorf("consul.domain is required"))
	}
	if c.DC == "" {
		errs = multierror.Append(errs, fmt.Errorf("consul.dc is required"))
	}
	if c.ServersCount < 0 {
		errs = multierror.Append(errs, fmt.Errorf("consul.servers_count must be positive"))
	}

	if c.JoinIP != "" && net.ParseIP(c.JoinIP) == nil {
		errs = multierror.Append(errs, fmt.Errorf("consul.joinip must be a valid IPv4"))
	}

	if c.JoinIPWan != "" && net.ParseIP(c.JoinIPWan) == nil {
		errs = multierror.Append(errs, fmt.Errorf("consul.joinip_wan must be a valid IPv4"))
	}
	return errs.ErrorOrNil()
}

func DefaultConfig() *Config {
	config := &Config{
		ServersCount: defaultServersCount,
		Domain:       defaultDomain,
		DC:           defaultDC,
	}
	return config
}
