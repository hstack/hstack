package assets

import (
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"
	"text/template"

	"gitlab.com/hstack/hstack/config"
)

var (
	templateFuncs map[string]interface{}
)

func RestoreTo(outputDir string, conf *config.Config) error {
	// checking OutputDir is a directory
	dir, err := os.Open(outputDir)
	if err != nil {
		return fmt.Errorf("error while accessing output-dir %q: %v", outputDir, err)
	}

	defer dir.Close()

	fstat, err := dir.Stat()
	if err != nil {
		return fmt.Errorf("error while accessing output-dir %q: %v", outputDir, err)
	}

	if !fstat.IsDir() {
		return fmt.Errorf("%q is not a directory!", dir.Name())
	}

	if err := RestoreAssets(path.Join(outputDir, conf.Name), "packer"); err != nil {
		return fmt.Errorf("Error restoring packer assests to %s: %v", outputDir, err)
	}

	providerPrefix := fmt.Sprintf("terraform/providers/%v", conf.Provider.(config.ProviderConfig).Type())
	scriptsPrefix := fmt.Sprintf("scripts/%v", conf.Provider.(config.ProviderConfig).Type())

	for _, asset := range AssetNames() {
		if strings.HasPrefix(asset, providerPrefix) ||
			strings.HasPrefix(asset, scriptsPrefix) ||
			strings.HasSuffix(asset, ".md") ||
			strings.HasSuffix(asset, ".md") ||
			strings.HasSuffix(asset, ".org") ||
			strings.HasSuffix(asset, ".txt") ||
			asset == ".gitignore" {
			if err := restoreAssetTemplateTo(asset, outputDir, conf); err != nil {
				return err
			}
		}
	}

	return nil
}

func restorePath(outputDir, assetName string, conf *config.Config) string {
	restorePath := path.Join(outputDir, conf.Name)
	for _, p := range strings.Split(assetName, "/") {
		if p == "region_id" {
			restorePath = path.Join(restorePath, conf.Provider.(config.ProviderConfig).Datacenter())
		} else {
			restorePath = path.Join(restorePath, p)
		}
	}
	return restorePath
}

func restoreAssetTemplateTo(assetName, outputDir string, conf *config.Config) error {
	data, err := Asset(assetName)
	if err != nil {
		return err
	}

	tmpl, err := template.New(assetName).Funcs(templateFuncs).Parse(string(data))

	if err != nil {
		return err
	}

	filePath := restorePath(outputDir, assetName, conf)

	err = os.MkdirAll(filepath.Dir(filePath), os.FileMode(0755))
	if err != nil {
		return err
	}

	file, err := os.Create(filePath)
	if err != nil {
		return err
	}

	if strings.HasSuffix(filePath, ".sh") {
		if err = os.Chmod(filePath, os.FileMode(0755)); err != nil {
			return err
		}
	}

	if err = tmpl.Execute(file, conf); err != nil {
		return err
	}
	return nil
}
