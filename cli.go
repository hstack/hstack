package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"sync"

	"gitlab.com/hstack/hstack/assets"
	"gitlab.com/hstack/hstack/config"
)

// Exit codes are int values that represent an exit code for a particular error.
// Sub-systems may check this unique error to determine the cause of an error
// without parsing the output or help text.
//
// Errors start at 10
const (
	ExitCodeOK    int = 0
	ExitCodeError     = 10 + iota
	ExitCodeInterrupt
	ExitCodeParseFlagsError
	ExitCodeConfigError
)

/// ------------------------- ///

// CLI is the main entry point for hstack
type CLI struct {
	sync.Mutex

	// outSteam and errStream are the standard out and standard error streams to
	// write messages from the CLI.
	outStream, errStream io.Writer
}

// NewCLI creates a new CLI object with the given stdout and stderr streams.
func NewCLI(out, err io.Writer) *CLI {
	return &CLI{
		outStream: out,
		errStream: err,
	}
}

// Run accepts a slice of arguments and returns an int representing the exit
// status from the command.
func (cli *CLI) Run(args []string) int {
	// Parse the flags
	conf, outputDir, version, err := cli.parseFlags(args[1:])
	if err != nil {
		if err == flag.ErrHelp {
			return 0
		}
		return cli.handleError(err, ExitCodeParseFlagsError)
	}

	// Setup the config
	conf, err = cli.setup(conf)
	if err != nil {
		return cli.handleError(err, ExitCodeConfigError)
	}

	// Print version information for debugging
	log.Printf("[INFO] %s", formattedVersion())

	// If the version was requested, return an "error" containing the version
	// information. This might sound weird, but most *nix applications actually
	// print their version on stderr anyway.
	if version {
		log.Printf("[DEBUG] (cli) version flag was given, exiting now")
		fmt.Fprintf(cli.errStream, "%s\n", formattedVersion())
		return ExitCodeOK
	}

	err = conf.Validate()

	if err != nil {
		return cli.handleError(err, ExitCodeConfigError)
	}

	log.Printf("[DEBUG] Generating terraform project with conf: %v\n", conf)

	if err := assets.RestoreTo(outputDir, conf); err != nil {
		return cli.handleError(err, ExitCodeError)
	}

	if err != nil {
		cli.handleError(err, ExitCodeError)
	}
	log.Println("enjoy your fresh terraform project!")
	return ExitCodeOK

}

// parseFlags is a helper function for parsing command line flags using Go's
// Flag library. This is extracted into a helper to keep the main function
// small, but it also makes writing tests for parsing command line arguments
// much easier and cleaner.
func (cli *CLI) parseFlags(args []string) (*config.Config, string, bool, error) {
	var version bool
	var outputDir string

	conf := config.DefaultConfig()
	conf.Version = hstackVersion()
	conf.Set("version")

	// Parse the flags and options
	flags := flag.NewFlagSet(Name, flag.ContinueOnError)
	flags.SetOutput(cli.errStream)
	flags.Usage = func() { fmt.Fprintf(cli.errStream, usage, Name) }

	flags.Var((funcVar)(func(s string) error {
		conf.Path = s
		conf.Set("path")
		return nil
	}), "config", "")

	flags.Var((funcVar)(func(s string) error {
		conf.Name = s
		conf.Set("name")
		return nil
	}), "name", "")

	flags.Var((funcVar)(func(s string) error {
		conf.Version = s
		conf.Set("version")
		return nil
	}), "hstack-version", "")

	flags.StringVar(&outputDir, "output-dir", ".", "")
	flags.StringVar(&outputDir, "o", ".", "")
	flags.BoolVar(&version, "v", false, "")
	flags.BoolVar(&version, "version", false, "")

	// If there was a parser error, stop
	if err := flags.Parse(args); err != nil {
		return nil, "", false, err
	}

	// Error if extra arguments are present
	args = flags.Args()
	if len(args) > 0 {
		return nil, "", false, fmt.Errorf("cli: extra argument(s): %q",
			args)
	}

	return conf, outputDir, version, nil
}

// handleError outputs the given error's Error() to the errStream and returns
// the given exit status.
func (cli *CLI) handleError(err error, status int) int {
	fmt.Fprintf(cli.errStream, "hstack returned errors:\n%s\n", err)
	return status
}

func (cli *CLI) setup(conf *config.Config) (*config.Config, error) {
	if conf.Version == "" {
		conf.Version = formattedVersion()
	}

	if conf.Path != "" {
		newConf, err := config.ConfigFromPath(conf.Path)
		if err != nil {
			return nil, err
		}

		// Merge ensuring that the CLI options still take precedence
		newConf.Merge(conf)
		conf = newConf
	}

	return conf, nil
}

const usage = `
Usage: %s [options]

  Outputs a terraform project bootstrapping a hashicorp stack
  (consul, nomad, vault) for various cloud providers

Options:

  -config=<path>
      Sets the path to a configuration file on disk

  -output-dir=<path>
      Sets the path of the output directory in which the project will be
      generated

  -name=<name>
      Sets the name of the terraform project

  -hstack-version=<version>
      Sets the hstack-version to use for the terraform project
      defaults to current version

  -v, -version
      Print the current version of hstack
`
