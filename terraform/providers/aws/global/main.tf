variable "region"            { default = "us-east-1" }
variable "name"              { default = "hstack" }
variable "iam_admins"        { default = "hstack-admin" }

provider "aws" {
  region = "${var.region}"
}

module "iam_admins" {
  source = "git::https://gitlab.com/hstack/tf-mod-aws-iam-group.git"

  name       = "${var.name}"
  users      = "${var.iam_admins}"
  policy     = <<EOF
{
  "Version"  : "2012-10-17",
  "Statement": [
    {
      "Effect"  : "Allow",
      "Action"  : "*",
      "Resource": "*"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "consul_discovery" {
    name = "${var.name}-consul-discovery"
    path = "/"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Stmt1468377974000",
            "Effect": "Allow",
            "Action": [
                "autoscaling:DescribeAutoScalingInstances",
                "autoscaling:DescribeAutoScalingGroups",
                "ec2:DescribeInstances"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
    EOF
}

resource "aws_iam_role" "consul" {
    name = "${var.name}-consul"
    assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
                "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_policy_attachment" "consul_discovery" {
    name = "${var.name}-consul-discovery"
    roles = ["${aws_iam_role.consul.name}"]
    policy_arn = "${aws_iam_policy.consul_discovery.arn}"
}

resource "aws_iam_instance_profile" "consul" {
    name = "${var.name}-consul"
    role = "${aws_iam_role.consul.name}"
}

{{ if .Terraform.EnableRemoteState }}
resource "aws_s3_bucket" "terraform_remote_states" {
  bucket = "tf-state-${var.name}"
  acl = "private"

  tags {
    Name = "${var.name}"
  }
}
{{ end }}

output "config" {
  value = <<CONFIG
Admin IAM:
  Admin Users: ${join("\n               ", formatlist("%s", split(",", module.iam_admins.users)))}

  Access IDs: ${join("\n              ", formatlist("%s", split(",", module.iam_admins.access_ids)))}

  Secret Keys: ${join("\n               ", formatlist("%s", split(",", module.iam_admins.secret_keys)))}

{{ if .Terraform.EnableRemoteState }}
Terraform Remote States bucket:
  Name: ${aws_s3_bucket.terraform_remote_states.id}
  ARN: ${aws_s3_bucket.terraform_remote_states.arn}
{{ end }}
CONFIG
}

output "iam_admin_users"       { value = "${module.iam_admins.users}" }
output "iam_admin_access_ids"  { value = "${module.iam_admins.access_ids}" }
output "iam_admin_secret_keys" { value = "${module.iam_admins.secret_keys}" }

{{ if .Terraform.EnableRemoteState }}
output "tf_remote_state_bucket_id" { value = "${aws_s3_bucket.terraform_remote_states.id}" }
output "tf_remote_state_bucket_arn" { value = "${aws_s3_bucket.terraform_remote_states.arn}" }
{{ end }}
