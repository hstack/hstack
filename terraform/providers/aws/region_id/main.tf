variable "name"            { default = "hstack" }
variable "hstack_version"  { default = "v0.1" }
variable "atlas_username"  { default = "hstack"}
variable "region"          { default = "us-east-1" }

variable "vpc_cidr"        { default = "10.100.0.0/16" }
variable "vpn_cidr"        { default = "192.168.255.0/24"}

variable "availability_zones" { default = "us-east-1a,us-east-1b,us-east-1c"}

variable "bastion_count"   { default = "1" }
variable "bastion_instance_type" { default = "t2.micro" }

variable "openvpn_instance_type"    { default = "t2.micro" }
variable "openvpn_push_dns_servers" { default = "" }

variable "nat_count" { default = "-1" } # -1 will create as many nat instances as number of az provided

# variable "root_ssl_key"      { }
# variable "root_ssl_cert"     { }
variable "consul_domain"             { default = "consul"}
variable "consul_dc"                 { default = "dc1" }
variable "consul_join_ipv4_addr"     { default = "" }
variable "consul_join_ipv4_addr_wan" { default = "" }
variable "consul_servers_count"      { default = "3" }
variable "consul_instance_type"      { default = "t2.medium"}
variable "consul_encrypt_key"        { }

# variable "vault_node_count"    { }
# variable "vault_instance_type" { }

# # currently only keybase ids are supported for pgp keys
# variable "vault_pgp_keys"      { }
# variable "vault_key_threshold" { }


# variable "influxdb_url"         { }
# variable "influxdb_name"        { }
# variable "influxdb_user"        { }
# variable "influxdb_password"    { }
# variable "gelf_endpoint"        { }

provider "aws" {
  region = "${var.region}"
}

# SSH KEYPAIRS
{{ range $keyname, $keyconf := .Ssh.Keypairs }}
{{ if $keyconf.Generated }}
resource "tls_private_key" "{{ $keyname }}" {
  algorithm   = "RSA"
}

resource "aws_key_pair" "{{ $keyname }}" {
  key_name   = "{{ $keyname }}"
  public_key = "${tls_private_key.{{ $keyname }}.public_key_openssh}"

  lifecycle { create_before_destroy = true }
}
{{ else }}
resource "aws_key_pair" "{{ $keyname }}" {
  key_name   = "{{ $keyname }}"
  public_key = "{{ $keyconf.PublicKey }}"

  lifecycle { create_before_destroy = true }
}
{{ end }}
{{ end }}

module "network" {
  source             = "git::https://gitlab.com/hstack/tf-mod-aws-network.git"
  name               = "${var.name}-network"
  region             = "${var.region}"
  vpc_cidr           = "${var.vpc_cidr}"
  availability_zones = "${var.availability_zones}"
  hstack_version     = "${var.hstack_version}"
  atlas_username     = "${var.atlas_username}"

  keypair_name       = "${aws_key_pair.{{ .Terraform.SshKeypair }}.key_name}"

  bastion_instance_type = "${var.bastion_instance_type}"
  bastion_count         = "${var.bastion_count}"
  nat_count             = "${var.nat_count}"
}

module "consul_servers" {
  source = "git::https://gitlab.com/hstack/tf-mod-aws-consul.git"

  name = "${var.name}-consul"
  region = "${var.region}"
  count = "${var.consul_servers_count}"
  instance_type = "${var.consul_instance_type}"
  hstack_version = "${var.hstack_version}"
  keypair_name       = "${aws_key_pair.{{ .Terraform.SshKeypair }}.key_name}"

  iam_instance_profile = "${var.name}-consul"
  domain = "${var.consul_domain}"
  dc = "${var.consul_dc}"

  subnet_ids = "${module.network.private_subnet_ids}"
  encrypt_key = "${var.consul_encrypt_key}"

  autojoin_tag_value = "${var.name}_consulserversjoin"
  join_ipv4_addr     = "${var.consul_join_ipv4_addr}"
  join_ipv4_addr_wan = "${var.consul_join_ipv4_addr_wan}"
}

module "openvpn" {
  source             = "git::https://gitlab.com/hstack/tf-mod-aws-openvpn.git"

  name               = "${var.name}-openvpn"
  region             = "${var.region}"
  atlas_username     = "${var.atlas_username}"
  hstack_version     = "${var.hstack_version}"
  vpc_id             = "${module.network.vpc_id}"
  vpn_cidr           = "${var.vpn_cidr}"
  public_subnet_ids  = "${module.network.public_subnet_ids}"
  private_subnet_ids = "${module.network.private_subnet_ids}"
  keypair_name       = "${aws_key_pair.{{ .Terraform.SshKeypair }}.key_name}"
  instance_type      = "${var.openvpn_instance_type}"


  iam_instance_profile = "${var.name}-consul"
  domain = "${var.consul_domain}"
  dc = "${var.consul_dc}"
  encrypt_key = "${var.consul_encrypt_key}"
  autojoin_tag_value = "${var.name}_consulserversjoin"
}

# module "data" {
#   source = "../../../modules/aws/data"

#   name               = "${var.name}"
#   hstack_version     = "${var.hstack_version}"

#   region             = "${var.region}"
#   vpc_id             = "${module.network.vpc_id}"
#   vpc_cidr           = "${var.vpc_cidr}"
#   private_subnet_ids = "${module.network.private_subnet_ids}"
#   public_subnet_ids  = "${module.network.public_subnet_ids}"
#   key_name           = "${aws_key_pair.hstack.key_name}"
#   atlas_username     = "${var.atlas_username}"
#   domain             = "${var.domain}"
#   sub_domain         = "${var.sub_domain}"

#   consul_node_count    = "${var.consul_node_count}"
#   consul_instance_type = "${var.consul_instance_type}"
#   consul_key           = "${var.consul_key}"
#   consul_joinip        = "${var.consul_joinip}"
#   consul_joinip_wan    = "${var.consul_joinip_wan}"

#   private_key          = "${file(var.ssh_private_key)}"
#   bastion_host         = "${module.network.bastion_elb_dns}"
#   bastion_user         = "${module.network.bastion_user}"

#   root_ssl_key         = "${var.root_ssl_key}"
#   root_ssl_cert        = "${var.root_ssl_cert}"

#   vault_node_count    = "${var.vault_node_count}"
#   vault_instance_type = "${var.vault_instance_type}"
#   vault_pgp_keys      = "${var.vault_pgp_keys}"
#   vault_key_threshold = "${var.vault_key_threshold}"

#   influxdb_url        = "${var.influxdb_url}"
#   influxdb_name       = "${var.influxdb_name}"
#   influxdb_user       = "${var.influxdb_user}"
#   influxdb_password   = "${var.influxdb_password}"
#   gelf_endpoint       = "${var.gelf_endpoint}"
# }


# module "compute" {
#   source = "../../../modules/aws/compute"

#   name               = "${var.name}"
#   region             = "${var.region}"
#   vpc_id             = "${module.network.vpc_id}"
#   vpc_cidr           = "${var.vpc_cidr}"
#   key_name           = "${aws_key_pair.hstack.key_name}"
#   availability_zones                = "${var.availability_zones}"
#   private_subnet_ids = "${module.network.private_subnet_ids}"
#   public_subnet_ids  = "${module.network.public_subnet_ids}"
#   atlas_username     = "${var.atlas_username}"
#   sub_domain         = "${var.sub_domain}"

# }

output "configuration" {
  value = <<CONFIGURATION

* Distribute & backup your ssh private key:
{{ range $keyname, $keyconf := .Ssh.Keypairs }}
{{ if $keyconf.Generated }}
key: {{ $keyname }}
   pub: ${tls_private_key.{{ $keyname }}.public_key_pem}
   ssh_pub: ${tls_private_key.{{ $keyname }}.public_key_openssh}
   priv: ${tls_private_key.{{ $keyname }}.private_key_pem}

{{ end }}
{{ end }}

* SSH into any private node via the Bastion host:
  $$ ssh-add [path_to_pub_key]
  $$ ssh -A core@${module.network.bastion_public_dns}

* Add these lines to your ~/.ssh/config file to connect to your instances through the bastion:
   ---
   Host "${replace(replace(var.vpc_cidr,".0",""),"/\\/[\\d]+/",".*")}"
      IdentityFile [path_to_ssh_private_key]
      User core
      ProxyCommand ssh -l core -i [path_to_ssh_private_key] -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null "${module.network.bastion_public_dns}" ncat %h %p
   ---

* Useful information:
  The VPC environment is accessible via:
     - Vpn : Public Address              : ${module.openvpn.elb_dns}

* Get an openvpn client config file:
  $$ ssh core@${module.openvpn.elb_dns} sudo /opt/hstack/ovpn-manage client [CLIENTNAME] > /tmp/[CLIENTNAME].cfg
  $$ sudo openvpn /tmp/[CLIENTNAME].cfg

  This should push dns servers to your system. If it doesn't, setup on of consul private ips in your dns service to serve all requests for domains ${var.consul_dc}.${var.consul_domain} or ${var.consul_domain}

Once you're on the VPN, you can...

CONFIGURATION
}
