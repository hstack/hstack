#--------------------------------------------------------------
# General
#--------------------------------------------------------------

name              = "{{.Name}}"
atlas_username    = "{{.Atlas.Username}}"
hstack_version    = "{{.Version}}"

region            = "{{ .Provider.Region }}"

#--------------------------------------------------------------
# Network
#--------------------------------------------------------------

vpc_cidr        = "{{ .Provider.Network }}"
vpn_cidr        = "{{ .Openvpn.ClientNetwork }}"
availability_zones = "{{ join_arr (map_keys .Provider.AvailabilityZones) "," }}" # AZs are region specific

# Bastion
bastion_instance_type = "{{ or (index .Provider.InstanceTypes "bastion") .Provider.DefaultInstanceType}}"

# NAT
nat_count         = "{{ .Provider.NatCount }}"
nat_instance_type = "{{ or (index .Provider.InstanceTypes "nat") .Provider.DefaultInstanceType}}"

# Openvpn
openvpn_instance_type = "{{ or (index .Provider.InstanceTypes "openvpn") .Provider.DefaultInstanceType}}"


# ## if no CA is provided, vault
# root_ssl_key      = "{{ .Vault.SslBackend.RootCA.Key }}"
# root_ssl_cert     = "{{ .Vault.SslBackend.RootCA.Cert }}"

#--------------------------------------------------------------
# Data
#--------------------------------------------------------------
# Consul
consul_servers_count      = "{{ .Consul.ServersCount }}"
consul_join_ipv4_addr     = "{{ .Consul.JoinIP }}"
consul_join_ipv4_addr_wan = "{{ .Consul.JoinIPWan }}"
consul_domain             = "{{ .Consul.Domain }}"
consul_dc                 = "{{ .Consul.DC }}"
consul_server_count       = "{{ .Consul.ServersCount }}"
consul_instance_type      = "{{ or (index .Provider.InstanceTypes "consul") .Provider.DefaultInstanceType}}"

# # Vault
# vault_node_count    = "{ { .Allocation.VaultServersCount }}"
# vault_instance_type = "{ { or (index .Provider.InstanceTypes "vault") .Provider.DefaultInstanceType}}"
# vault_pgp_keys      = "{ { .Allocation.VaultPgpKeys }}"
# vault_key_threshold = "{ { .Allocation.VaultKeyThreshold }}"

# #--------------------------------------------------------------
# # Compute
# #--------------------------------------------------------------



# #--------------------------------------------------------------
# # Monitoring
# #--------------------------------------------------------------
# influxdb_url        = "{ { .Allocation.InfluxDBUrl }}"
# influxdb_name       = "{ { .Allocation.InfluxDBName }}"
# influxdb_user       = "{ { .Allocation.InfluxDBUser }}"
# influxdb_password   = "{ { .Allocation.InfluxDBPassword }}"
# gelf_endpoint       = "{ { .Allocation.GelfEndpoint }}"

