package utils

// FlattenKeys is a function that takes a map[string]interface{} and recursively
// flattens any keys that are a []map[string]interface{} where the key is in the
// given list of keys.
func FlattenKeys(m map[string]interface{}) {
	var flatten func(map[string]interface{})
	flatten = func(m map[string]interface{}) {
		for k, v := range m {
			switch typed := v.(type) {
			case []map[string]interface{}:
				l := len(typed)
				if l == 1 {
					flatten(typed[0])
					m[k] = typed[0]
				} else if l > 1 {
					nv := make(map[string]interface{})
					for _, m := range typed {
						for kk, vv := range m {
							switch ttyped := vv.(type) {
							case []map[string]interface{}:
								if len(ttyped) > 0 {
									flatten(ttyped[len(ttyped)-1])
									nv[kk] = ttyped[len(ttyped)-1]
								} else {
									nv[kk] = nil
								}
							default:
								nv[kk] = ttyped
							}
						}
					}
					m[k] = nv
				} else {
					m[k] = nil
				}
			case map[string]interface{}:
				flatten(typed)
				m[k] = typed
			default:
				m[k] = v
			}
		}
	}

	flatten(m)

}
