# name = "demo"
# global_tags = []

atlas {
  username = "hstack"
}

terraform {
  enable_remote_state = false
  ssh_keypair = "terraform"
}

 consul {
   servers_count = 4
#   domain = "consul"
#   dc = "dc1"
 }

 vault {
#   servers_count = 3
   pgp_keys = ["keybase:parasitid"]
   key_threshold = 1

#   ssl_backend {
#     root_ca {
#       generated = true
#      }
#   }
 }

# nomad {
#   servers_count = 3
# }

 openvpn {
#   password = "dUswCYGXnPWw9J/e+k5c9xhZ7DA="
#   client_network = "192.168.255.0/24"
 }


ssh {
  keypair "ydegat" {
     generated = true
  }

  keypair "terraform" {
    generated = true
  }

  authorized_keys = ["terraform", "ydegat"]
}

# private_docker_registry {
#   secret = "XXXX"
# }

# telegraf {
#   influxdb {
#     url = ""
#     name = ""
#     user = ""
#     password = ""
#   }
# }

# nodes "mgmt" {
#   count = 3
#   tags = []
# }

provider {
  type = "aws"
  region = "us-east-2"

  nat_count = 1

  instance_types {
    bastion = "t2.micro"
    nat = "t2.micro"
    openvpn = "t2.micro"
  }

  default_instance_type = "t2.medium"

  availability_zone "us-east-2a" {}
  availability_zone "us-east-2b" {}
  availability_zone "us-east-2c" {}
}
