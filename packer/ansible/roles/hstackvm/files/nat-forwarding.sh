#!/bin/bash
source "$(dirname "$0")/functions.sh"
NAT_MODE=${NAT_MODE:-"off"}

if [ "$NAT_MODE" == "off" ]; then
    log user.info "NAT is off"
    exit 0
fi

IFS=,
for i in $NAT_SUBNETS; do
  log user.info "install nat forwarding for subnet $s"
  iptables -t nat -A POSTROUTING -o eth0 -s $i -j MASQUERADE
done

