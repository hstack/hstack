#!/bin/bash
# shellcheck source=/dev/null
source "$(dirname "$0")/functions.sh"

watch(){
    consulbin watch $(consulopts) -type=service -service="${VAULT_SECURED_SERVICE_NAME}" -passingonly=true "${HSTACK_HOME}/consul-watch-vault-handler"
}

handler(){
    # watch out! sometimes a node hasn't yet been marked has failing because the registered checks
    # haven't yet been played on the agent. must double check that vault is really passing
    # instead of checking input from consul
    if servicecheck "$VAULT_SECURED_SERVICE_NAME" "passing"; then
        log user.info "vault is tls enabled."
        if ! consulsecon; then
            log user.info "consul is not tls enabled, switching to sec conf."
            if "${HSTACK_HOME}/consul-manage" renew-certs; then
                "${HSTACK_HOME}/consul-manage" sec-conf
                "${HSTACK_HOME}/consul-manage" restart
            fi
        fi
    fi
}

# restarting consul triggers a vault restart, which then triggers a consul restart when vault is unsealed....
# thus this handler must not be registered on vault server nodes
if [ "$VAULT_MODE" == "server" ]; then
    log user.info "consul agent tls conf is handled by the vault watch on vault servers nodes. nothing to do."
    exit 0
fi

case $1 in
    watch)
        log user.info "watch"
        watch
        ;;
    *)
        handler
        ;;
esac
