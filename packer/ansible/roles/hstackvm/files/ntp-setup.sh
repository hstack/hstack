#!/bin/bash
# shellcheck source=/dev/null
source "$(dirname "$0")/functions.sh"
NTP_MODE=${NTP_MODE:-off}
ADMIN_IP=$(getprivipaddr)
PEERS_FILE=${PEERS_FILE:-"${HSTACK_CONFDIR}/ntp_peers"}

if [ -z "$NTP_SERVERS" ] && [ "$NTP_MODE" != "server" ]; then
    log user.info "no ntp setup. nothing to to."
    exit 0
fi

if [ "$NTP_MODE" == "server" ]; then
    log user.info "node is a ntp server."
    tee /tmp/ntp.conf <<EOF
#Common Pool
server 0.coreos.pool.ntp.org iburst
server 1.coreos.pool.ntp.org iburst
server 2.coreos.pool.ntp.org iburst
server 3.coreos.pool.ntp.org iburst

#Server Pool
EOF
    echo "peer $ADMIN_IP iburst" >> /tmp/ntp.conf

    IFS=,
    for i in $NTP_SERVERS; do
        if [ "$i" != "$ADMIN_IP" ]; then
            echo "peer $i iburst" >> /tmp/ntp.conf
        fi
    done

    if [ -f $PEERS_FILE ]; then
        for i in "$(cat $PEERS_FILE)"; do
            if [ "$i" != "$ADMIN_IP" ]; then
                echo "peer $i iburst" >> /tmp/ntp.conf
            fi
        done
    fi

    echo "restrict  ${PRIVATE_NETWORK%/*} mask $(cidr2mask ${PRIVATE_NETWORK#*/})" >> /tmp/ntp.conf
else
    tee /tmp/ntp.conf <<EOF
#Server Pool
EOF
    IFS=,
    for i in $NTP_SERVERS; do
        echo "server $i iburst" >> /tmp/ntp.conf
    done

    echo "restrict  ${PRIVATE_NETWORK%/*} mask $(cidr2mask ${PRIVATE_NETWORK#*/})" >> /tmp/ntp.conf
fi

cp -f /tmp/ntp.conf /etc/ntp.conf
systemctl stop systemd-timesyncd
systemctl mask systemd-timesyncd
systemctl enable ntpd
systemctl start ntpd

