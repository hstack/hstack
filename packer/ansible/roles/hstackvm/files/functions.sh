#!/bin/bash
# shellcheck source=/dev/null
if [ -f "/etc/environment" ]; then
    source /etc/environment
fi

log(){
    if ! tty -s; then logger -s -t "$SERVICE_NAME" -p "$@"; else echo "$@" >&2; fi;
}

if [ -f "/opt/hstack/defaults.conf" ]; then
    log user.info "loading default conf"
    source /opt/hstack/defaults.conf
else
    log user.error "couldn't find default configuration file."
    exit 1
fi

# load conf files
for i in /etc/hstack/*.conf; do
    if [ -e "$i" ]; then
        log user.info "loading $i"
        source $i
    fi
done

SCRIPT_FILE="$0"
if [[ "$SCRIPT_FILE" == "sh" ]] || [[ "$SCRIPT_FILE" == "-bash" ]] || [[ "$SCRIPT_FILE" == "-zsh" ]]; then
    echo "sourcing from sh|bash|zsh" >&2
    echo "SERVICE_NAME will be set to shell_$$".
    SERVICE_NAME="shell_$$"
    UUID_FILE=/var/run/"$SERVICE_NAME".uuid
else
    FILENAME="$(basename "$0")"
    SERVICE_NAME="${FILENAME%*-manage}"
    SERVICE_NAME="${SERVICE_NAME%*.sh}"
    UUID_FILE=/var/run/"$SERVICE_NAME".uuid
fi

getipaddrfornetwork(){
    NETWORK="$1"
    # Keep trying to retrieve IP addr until it succeeds. Timeouts after 1m
    now=$(date +%s)
    timeout=$(( now + 60 ))
    set +e
    while :; do
        if [[ $timeout -lt $(date +%s) ]]; then
            log user.error "Could not retrieve IP Address. Exiting"
            exit 5
        fi
        if ip route get "$NETWORK" > /dev/null 2>&1; then
          break
        fi
        sleep 1
    done
    ip -o route get "$NETWORK" | sed 's/.*src \([0-9\.]*\) .*/\1/g'
}

althostname(){
    getprivipaddr | sed "s/\./-/g"
}

getpubipaddr(){
    if [ ! -z "$COREOS_PUBLIC_IPV4" ]; then
        echo "$COREOS_PUBLIC_IPV4"
    else
        getipaddrfornetwork "${PUBLIC_NETWORK:-8.8.8.8/32}"
    fi
}

getprivipaddr(){
    if [ ! -z "$PRIVATE_NETWORK" ]; then
        getipaddrfornetwork "$PRIVATE_NETWORK"
    elif [ ! -z "$COREOS_PRIVATE_IPV4" ]; then
        echo "$COREOS_PRIVATE_IPV4"
    else
        getpubipaddr
    fi
}

exec_rkt(){
    if [ -f "$UUID_FILE" ]; then
        rkt enter "$(cat "$UUID_FILE")" "$@"
    else
        log user.error "no uuid. maybe container is not running."
    fi
}

kill_rkt() {
    if [ -f "$UUID_FILE" ]; then
        sudo machinectl -s "${1:QUIT}" kill "rkt-$(cat "$UUID_FILE")"
    fi
}

stop_rkt() {
    if [ -f "$UUID_FILE" ]; then
        sudo machinectl kill "rkt-$(cat "$UUID_FILE")"
    fi
}

rm_rkt() {
    if [ -f "$UUID_FILE" ]; then
        rkt rm --uuid-file "$UUID_FILE"
    fi
}

consullockopts(){
    LOCK_OPTS=()
    LOCK_OPTS+=(-timeout 30m)
    LOCK_OPTS+=(-n 1)
    LOCK_OPTS+=(-monitor-retry 3)
    LOCK_OPTS+=(-name "$(hostname)")
    echo "${LOCK_OPTS[@]}"
}

consulunsechttp() {
    METHOD=$1
    path=$2
    shift 2
    /usr/bin/curl --silent --fail \
                  -X"$METHOD" \
                  --connect-timeout 1 \
                  "http://127.0.0.1:8500/v1$path" "$@"
}

consulsechttp() {
    METHOD=$1
    path=$2
    shift 2
    /usr/bin/curl --silent --fail \
                  -X"$METHOD" \
                  --connect-timeout 1 \
                  --cacert "${HSTACK_CONFDIR}/certs/ca.pem" \
                  "https://127.0.0.1:8503/v1$path" "$@"
}

consulhttp(){
    if consulsecon; then
        consulsechttp "$@"
    else
        consulunsechttp "$@"
    fi
}

consulopts(){
    CONSUL_OPTS=()
    if consulsecon; then
        # meaning vault has been bootstrapped and https can&must be used
        CONSUL_OPTS+=(-ca-file "${ROOT_CA_PATH:-error}")
        #CONSUL_OPTS+=(-client-cert "${HSTACK_CONFDIR}/certs/consul-client.pem")
        #CONSUL_OPTS+=(-client-key "${HSTACK_CONFDIR}/certs/consul-client-key.pem")
        CONSUL_OPTS+=(-http-addr https://127.0.0.1:8503)
    else
        CONSUL_OPTS+=(-http-addr http://127.0.0.1:8500)
    fi
    echo "${CONSUL_OPTS[*]}"
}

consulsecon(){
    ncat -z 127.0.0.1 8503
}

nomadsecon(){
    curl --silent --fail \
         --connect-timeout 1 \
         --cacert "${HSTACK_CONFDIR}/certs/ca.pem" \
         https://127.0.0.1:4646/v1/status/leader
}

nomadunsecon(){
    curl --silent --fail \
         --connect-timeout 1 \
         http://127.0.0.1:4646/v1/status/leader
}

consulbin() {
    "$CONSUL_BIN" "$@"
}

nomadbin() {
    "$NOMAD_BIN" "$@"
}

consulkvunlock(){
    LOCK=$1
    SESSION_ID=$(cat /var/run/"$LOCK".consulsession)
    if [ ! -z "$SESSION_ID" ]; then
        if ! consulbin kv put -release=true -session="$SESSION_ID" "locks/$LOCK"; then
            log user.error "lock already released".
            return 1
        else
            log user.info "lock released for session $SESSION_ID"
        fi
    fi
}

consulwait4leader(){
    local timeout=$1
    local start=1
    while true; do
        if consulhttp GET /status/leader | grep -q ".*:8300"; then
            break
        elif [ "$start" -gt "$timeout" ]; then
            return 1
        fi
        sleep 1
        ((start++))
    done
}

nomadwait4leader(){
    local timeout=$1
    local start=1
    while true; do
        if consulbin operator raft list-peers | grep -q "leader"; then
            break
        elif [ "$start" -gt "$timeout" ]; then
            return 1
        fi
        sleep 1
        ((start++))
    done
}

consulhttpnewsession(){
    NAME=$1
    consulhttp PUT "/session/create" -d '{"Name":"'$(althostname)'", "TTL": "120s", "LockDelay" : "120s"}' 2>/dev/null | jq -r '.ID' | tee /var/run/"$NAME".consulsession
}

consulkvlock(){
    LOCK=$1
    touch /var/run/"$LOCK".consulsession
    SESSION_ID=$(cat /var/run/"$LOCK".consulsession)
    if [ ! -z "$SESSION_ID" ]; then
        if ! consulhttp PUT "/session/renew/$SESSION_ID"; then
            SESSION_ID=$(consulhttpnewsession "$LOCK")
        fi
    else
        SESSION_ID=$(consulhttpnewsession "$LOCK")
    fi

    if ! consulbin kv put -acquire=true -session="$SESSION_ID" "locks/$LOCK"; then
        log user.error "could not acquire lock locks/$LOCK for session $SESSION_ID".
        return 1
    else
        log user.info "lock locks/$LOCK acquired for session $SESSION_ID"
    fi
}

consulwait4kvlock(){
    local timeout=$1
    local lock=$2
    local start=1
    while true; do
        if consulkvlock $2; then
            break
        elif [ "$start" -gt "$timeout" ]; then
            return 1
        fi
        sleep 1
        ((start++))
    done
}

servicecheck(){
    consulunsechttp GET "/health/checks/$1" | jq '.[].Status' | grep -q "${2:-passing}"
}

localservicecheck(){
    consulunsechttp GET "/health/checks/$1" | jq '.|map(select(.Node == "'$(althostname)'"))|.[].Status' | grep -q "${2:-passing}"
}

vaulthttp() {
    METHOD=$1
    path=$2
    shift 2
    case $METHOD in
        PUT|POST)
            JSON_OPTS=()
            for i in "$@"; do
                JSON_OPTS+=("$(arg2jsonattr $i)")
            done

            /usr/bin/curl --silent --fail \
                          -X"$METHOD" \
                          -H "X-Vault-Token:$VAULT_TOKEN" \
                          "${VAULT_ADDR}/v1$path" \
                          -d "{$(joinby , "${JSON_OPTS[@]}")}"
            ;;
        GET)
            /usr/bin/curl --silent --fail \
                          -X"$METHOD" \
                          -H "X-Vault-Token:$VAULT_TOKEN" \
                          "${VAULT_ADDR}/v1$path"
            ;;
    esac
}

vaultopts(){
    VAULT_OPTS=()
    if servicecheck "$VAULT_SECURED_SERVICE_NAME" "passing"; then
        # meaning vault has been bootstrapped and https can & must be used
        VAULT_OPTS+=(-ca-cert "${ROOT_CA_PATH:-error}")
        #VAULT_OPTS+=(-client-cert "${HSTACK_CONFDIR}/certs/vault-client.pem")
        #VAULT_OPTS+=(-client-key "${HSTACK_CONFDIR}/certs/vault-client-key.pem")
        VAULT_OPTS+=(-address "https://${VAULT_SECURED_SERVICE_NAME}.service.${DATACENTER}.${DOMAIN}:8200")
    elif [ "$VAULT_MODE" == "server" ]; then
        VAULT_OPTS+=(-address "http://127.0.0.1:8200")
    else
        log user.error "vault is not ready."
    fi

    echo "${VAULT_OPTS[@]}"
}

vaultbin() {
    "$VAULT_BIN" "$@"
}

nomadbin() {
    "$NOMAD_BIN" "$@"
}

get_vault_root_token_from_consul(){
    consulbin kv get $(consulopts) "${CONSULKEY_VAULT_ROOT_TOKEN}"
}

get_vault_token(){
    local app_role_id="$(fail consulbin kv get $(consulopts) vault/auth/nodes/role/node/role-id)"

    if [ -z "$1" ]; then
        vaultbin write $(vaultopts) -field=token auth/nodes/login role_id="${app_role_id}"
    else
        export VAULT_TOKEN="$(vaultbin write $(vaultopts) -field=token auth/nodes/login role_id="${app_role_id}")"
        vaultbin token-create $(vaultopts) -role="${1}" -format json | jq -r '.auth.client_token'
        unset VAULT_TOKEN
    fi
}

aci_name(){
    if [ ! -f "$HSTACK_HOME"/acis/image.list ]; then
        echo "image.list not found in $HSTACK_HOME/acis">&2
        exit 1
    fi

    IMAGE_NAME=$(grep "^$1:.*" "$HSTACK_HOME"/acis/image.list)
    if [ -z "$IMAGE_NAME" ]; then
        echo "image not found in $HSTACK_HOME/acis/image.list">&2
        exit 1
    fi

    ACI_NAME=$(rkt image list --fields=name | grep "$IMAGE_NAME")
    if [ -z "$ACI_NAME" ]; then
        echo "aci $ACI_NAME not found in rkt image list">&2
        exit 1
    fi
    echo "$ACI_NAME"
}

aci_id(){
    if [ ! -f "$HSTACK_HOME"/acis/image.list ]; then
        echo "image.list not found in $HSTACK_HOME/acis">&2
        exit 1
    fi

    IMAGE_UID=$(grep "^$1:.* .*" "$HSTACK_HOME"/acis/image.list | awk '{print $2}')
    if [ -z "$IMAGE_UID" ]; then
        echo "image not found in $HSTACK_HOME/acis/image.list">&2
        exit 1
    fi

    ACI_ID=$(rkt image list --fields=id | awk '$0~v {print $1}' v="${IMAGE_UID:0:19}" | head -1)
    if [ -z "$ACI_ID" ]; then
        echo "aci $ACI_ID not found in rkt image list">&2
        exit 1
    fi
    echo "$ACI_ID"
}

cidr2mask() {
    CIDR=${1#*/}
    local i mask=""
    local full_octets=$((CIDR/8))
    local partial_octet=$((CIDR%8))

    for ((i=0;i<4;i+=1)); do
        if [ $i -lt $full_octets ]; then
            mask+=255
        elif [ $i -eq $full_octets ]; then
            mask+=$((256 - 2**(8-partial_octet)))
        else
            mask+=0
        fi
        test $i -lt 3 && mask+=.
    done

    echo $mask
}

mask2cidr() {
    nbits=0
    IFS=.
    for dec in $1 ; do
        case $dec in
            255) let nbits+=8;;
            254) let nbits+=7;;
            252) let nbits+=6;;
            248) let nbits+=5;;
            240) let nbits+=4;;
            224) let nbits+=3;;
            192) let nbits+=2;;
            128) let nbits+=1;;
            0);;
            *) echo "Error: $dec is not recognised"; exit 1
        esac
    done
    echo "$nbits"
}

ispositiveint(){
    re='^[0-9]+$'
    [[ $1 =~ $re ]]
}

function joinby {
    local IFS="$1";
    shift;
    echo "$*";
}

function arg2jsonattr(){
    #shellcheck disable=SC2001
    attr="$(echo $1 | sed 's/^\([^=]*\)=\(.*\)$/\1/g')"
    #shellcheck disable=SC2001
    value="$(echo $1 | sed 's/^\([^=]*\)=\(.*\)$/\2/g')"
    #shellcheck disable=SC2001
    arrvalue="$(echo $1 | sed 's/^\([^=]*\)=\[\([[:alnum:],]*\)\]$/\2/g')"

    if [ ! -z "$arrvalue" ] && [ "$arrvalue" != "$1" ]; then
        values=()
        local IFS=",";
        for v in $arrvalue; do
            values+=("\"$v\"")
        done
        echo "\"$attr\":[$(joinby , ${values[*]})]"
    else
        echo "\"$attr\":\"$value\""
    fi
}

fail(){
    set -e
    "$@"
    set +e
}
