#!/bin/bash
IMAGE="$1"
uid=$(sudo rkt fetch --insecure-options=image "docker://$IMAGE")
echo "$IMAGE $uid" | sudo tee -a /opt/hstack/acis/image.list
sudo rkt image export --overwrite "$uid" "/opt/hstack/acis/$uid.aci"

