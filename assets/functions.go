package assets

import (
	"fmt"
	"reflect"
	"strings"
	"text/template"
)

func init() {
	templateFuncs = template.FuncMap{
		"map_keys": mapKeys,
		"map_attr": mapAttr,
		"join":     join,
		"join_arr": strings.Join,
		"mindex":   mindex,
	}
}

func join(s ...string) string {
	// first arg is sep, remaining args are strings to join
	return strings.Join(s[1:], s[0])
}

func mapKeys(m interface{}) []string {
	v := reflect.ValueOf(m)

	switch v.Kind() {
	case reflect.Map:
		res := make([]string, 0)
		for _, k := range v.MapKeys() {
			res = append(res, fmt.Sprintf("%v", k))
		}
		return res
	default:
		fmt.Printf("sdlfkjsfl %T\n", m)
		return nil
	}
}

func mapAttr(m interface{}, attr string) []string {
	v := reflect.ValueOf(m)

	switch v.Kind() {
	case reflect.Map:
		switch v.Kind() {
		case reflect.Map:
			res := make([]string, 0)
			for _, k := range v.MapKeys() {
				e := v.MapIndex(k).Elem()
				typeOfE := e.Type()
				for i := 0; i < e.NumField(); i++ {
					f := e.Field(i)
					if typeOfE.Field(i).Name == attr {
						res = append(res, fmt.Sprintf("%v", f.Interface()))
					}
				}
			}
			return res
		default:
			return nil
		}
	default:
		return nil
	}
}

func mindex(opts map[string]interface{}, s ...string) interface{} {
	if opts == nil {
		return nil
	}

	if len(s) == 0 {
		return opts
	}

	if v := opts[s[0]]; v != nil {
		switch typed := v.(type) {
		case map[string]interface{}:
			return mindex(typed, s[1:]...)
		default:
			if len(s) == 1 {
				return typed
			} else {
				return nil
			}
		}

	} else {
		return nil
	}
}
