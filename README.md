HSTACK
===============

This project provides a convenient way to scaffold a complete [Terraform][] project for various cloud providers, providing a setup for the following [HashiCorp][] tools:

- [Consul][]
- [Vault][]
- [Nomad][]

The terraform configuration tries to follow the [HashiCorp best practice project](https://github.com/hashicorp/best-practices).


Description
-----------
[hstack] takes a config file in input ( see [examples/config.hcl](https://gitlab.com/hstack/hstack/blob/master/examples/config.hcl) ) and outputs a terraform project based on the templates in [terraform](https://gitlab.com/hstack/hstack/blob/master/terraform) which are embedded in the binary thanks to [Go bindata].
The terraform scripts make heavy use of various terraform modules such as [hstack/tf-mod-aws-network](https://gitlab.com/hstack/tf-mod-aws-network), [hstack/tf-mod-aws-consul](https://gitlab.com/hstack/tf-mod-aws-consul), [hstack/tf-mod-aws-openvpn](https://gitlab.com/hstack/tf-mod-aws-openvpn), ...

TODO: Write at least one tf module by [HashiCorp] tool ( [Nomad], [Vault], [Consul] ) and per provider ( aws, azure, gcp, openstack ... )

Each module makes use of a VM which is build by [Packer] and registered on [Atlas].
The packer project is also embedded in the [hstack] binary and output next to the terraform project.

The config file mainly takes information on:
- consul/vault/nomad servers setup,  namely "control nodes"
- consul+nomad agents setup,  namely "compute nodes"

One should be able to generate a terraform project with control nodes only, control nodes + agent nodes, agent nodes only and join these clusters by properly setting consul parameters (autojoin, joinip, ... )
x

Installation
------------

Currently, htack is only available by compiling from source, please see the instructions in the [Contributing](#contributing) section.

Usage
-----
### Options
For the full list of options that correspond with your release, run:

```shell
hstack -h
```

### Command Line

### Configuration File(s)
The hstack configuration files are written in [HashiCorp Configuration Language (HCL)][HCL]. By proxy, this means the hstack configuration file is JSON-compatible. For more information, please see the [HCL specification][HCL].

The Configuration file syntax interface supports all of the options detailed above, unless otherwise noted in the table.

```javascript
# name = "demo"
# global_tags = []

atlas {
  username = "hstack"
}

terraform {
  enable_remote_state = false
  ssh_keypair = "terraform"
}

 consul {
   servers_count = 4
#   domain = "consul"
#   dc = "dc1"
 }

 vault {
#   servers_count = 3
   pgp_keys = ["keybase:parasitid"]
   key_threshold = 1

#   ssl_backend {
#     root_ca {
#       generated = true
#      }
#   }
 }

# nomad {
#   servers_count = 3
# }

 openvpn {
   client_network = "192.168.255.0/24"
 }


ssh {
  keypair "terraform" {
    generated = true
  }

  authorized_keys = ["terraform"]
}


provider {
  type = "aws"
  region = "us-east-2"

  nat_count = 1

  instance_types {
    bastion = "t2.micro"
    nat = "t2.micro"
    openvpn = "t2.micro"
  }

  default_instance_type = "t2.medium"

  availability_zone "us-east-2a" {}
  availability_zone "us-east-2b" {}
  availability_zone "us-east-2c" {}
}
```

Contributing
------------
To build and install hstack locally, you will need a modern [Go][] (Go 1.5+) environment.

First, clone the repo:

```shell
$ git clone https://gitlab.com/hstack/hstack.git
```

Next, download/update all the dependencies:

```shell
$ make updatedeps
```

To compile the `hstack` binary and run the test suite:

```shell
$ make dev
```

This will compile the `hstack` binary into `bin/hstack` as well as your `$GOPATH` and run the test suite.


Submit Pull Requests and Issues to the [Hstack project on Gitlab][hstack].


[HashiCorp]: https://www.hashicorp.com/ "Provision, Secure, Connect and Run any infrastructure for any application."
[Atlas]: https://atlas.hashicorp.com/ "Hashicorp cloud"
[Consul]: https://www.consul.io/ "Service discovery and configuration made easy"
[Packer]: https://www.packer.io/ "Build Automated Machine Images"
[Nomad]: https://nomadproject.io/ "Easily deploy applications at any scale"
[Vault]: https://vaultproject.io/ "A Tool for Managing Secrets"
[Terraform]: https://terraform.io/ "Write, Plan, and Create Infrastructure as Code"
[HCL]: https://github.com/hashicorp/hcl "HashiCorp Configuration Language (HCL)"
[Go]: https://golang.org "Go the language"
[hstack]: https://gitlab.com/hstack/hstack "this page"
[Go Template]: https://golang.org/pkg/text/template/ "Go Template"
[Go Bindata]: https://github.com/jteeuwen/go-bindata "A small utility which generates Go code from any file. Useful for embedding binary data in a Go program"
