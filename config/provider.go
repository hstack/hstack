package config

type ProviderConfig interface {
	Type() string
	Datacenter() string
	Validate() error
}
