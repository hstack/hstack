package aws

import (
	"fmt"
	"net"

	"github.com/hashicorp/go-multierror"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/hstack/hstack/utils"
)

// defaultNetwork is the default network used by the terraform project
const defaultRegion = "us-east-1"

// defaultNetwork is the default network used by the terraform project
const defaultNetwork = "10.235.1.0/24"

// defaultInstanceType is the default instance type used for aws instances
const defaultInstanceType = "t2.medium"

const providerTypeName = "aws"

// defaultNatCount is the default number of nat gateway that will be provisioned
const defaultNatCount = 1

// Config is used to configure hss projets outputs
type Config struct {

	// Type is the type of the provider
	TypeName string `mapstructure:"type"`

	// Region is the aws region
	Region string `mapstructure:"region"`

	// Network is the main subnet used by the terraform project
	Network string `mapstructure:"network"`

	// NatCount is the number of nat gateway that will be provisioned
	NatCount int `mapstructure:"nat_count"`

	// availability_zones are the availability_zones in which the resources
	// will be deployed
	AvailabilityZones map[string]*AvailabilityZoneConfig `mapstructure:"availability_zone"`

	// instance_types are the instance type per component (bastion, vpn, node, ...)
	InstanceTypes map[string]string `mapstructure:"instance_types"`

	// default_instance_type is the instance type used by default if an instance type
	// is not found for a component in InstanceTypes
	DefaultInstanceType string `mapstructure:"default_instance_type"`

	// setKeys is the list of config keys that were set by the user.
	setKeys map[string]struct{}
}

func (c *Config) String() string {
	return fmt.Sprintf(`{
	TypeName: %s,
	Region: %s,
	Network: %s,
	NatCount: %d,
	AvailabilityZones: %v,
	DefaultInstanceType: %v,
	InstanceTypes: %v,
},
`,
		c.TypeName,
		c.Region,
		c.Network,
		c.NatCount,
		c.AvailabilityZones,
		c.DefaultInstanceType,
		c.InstanceTypes,
	)
}

func (c *Config) Datacenter() string {
	return c.Region
}

func (c *Config) Type() string {
	return providerTypeName
}

// Copy returns a deep copy of the current configuration. This is useful because
// the nested data structures may be shared.
func (c *Config) Copy() *Config {
	config := new(Config)
	config.Region = c.Region
	config.Network = c.Network
	config.TypeName = c.TypeName
	config.NatCount = c.NatCount
	config.DefaultInstanceType = c.DefaultInstanceType

	if c.AvailabilityZones != nil {
		config.AvailabilityZones = make(map[string]*AvailabilityZoneConfig)
		for k, v := range c.AvailabilityZones {
			config.AvailabilityZones[k] = v.Copy()
		}
	}

	if c.InstanceTypes != nil {
		config.InstanceTypes = make(map[string]string)
		for k, v := range c.InstanceTypes {
			config.InstanceTypes[k] = v
		}
	}

	config.setKeys = c.setKeys
	return config
}

// Merge merges the values in config into this config object. Values in the
// config object overwrite the values in c.
func (c *Config) Merge(config *Config) {
	if config.WasSet("region") {
		c.Region = config.Region
	}

	if config.WasSet("network") {
		c.Network = config.Network
	}

	if config.WasSet("type") {
		c.TypeName = config.TypeName
	}

	if config.WasSet("nat_count") {
		c.NatCount = config.NatCount
	}

	if config.WasSet("default_instance_type") {
		c.DefaultInstanceType = config.DefaultInstanceType
	}

	if config.WasSet("availability_zone") {
		if c.AvailabilityZones == nil {
			c.AvailabilityZones = make(map[string]*AvailabilityZoneConfig)
		}
		for k, v := range config.AvailabilityZones {
			c.AvailabilityZones[k] = v
		}
	}

	if config.WasSet("instance_types") {
		if c.InstanceTypes == nil {
			c.InstanceTypes = make(map[string]string)
		}
		for k, v := range config.InstanceTypes {
			c.InstanceTypes[k] = v
		}
	}

	if c.setKeys == nil {
		c.setKeys = make(map[string]struct{})
	}
	for k := range config.setKeys {
		if _, ok := c.setKeys[k]; !ok {
			c.setKeys[k] = struct{}{}
		}
	}
}

// WasSet determines if the given key was set in the config (as opposed to just
// having the default value).
func (c *Config) WasSet(key string) bool {
	if _, ok := c.setKeys[key]; ok {
		return true
	}
	return false
}

// set is a helper function for marking a key as set.
func (c *Config) Set(key string) {
	if _, ok := c.setKeys[key]; !ok {
		c.setKeys[key] = struct{}{}
	}
}

func (c *Config) Validate() error {
	var errs *multierror.Error

	if c.TypeName != providerTypeName {
		errs = multierror.Append(errs, fmt.Errorf("wrong type name"))
		return errs.ErrorOrNil()
	}

	if c.Region == "" {
		errs = multierror.Append(errs, fmt.Errorf("region is required"))
	}

	_, _, err := net.ParseCIDR(c.Network)
	if err != nil {
		errs = multierror.Append(errs,
			fmt.Errorf("network is required and must be a valid IPv4 CIDR"))
	}

	if c.AvailabilityZones == nil {
		errs = multierror.Append(errs, fmt.Errorf("at least one availability_zone is required"))
	} else {
		for k, v := range c.AvailabilityZones {
			if err := v.Validate(); err != nil {
				errs = multierror.Append(errs, fmt.Errorf("availability_zone[%s]: %v", k, err))
			}
		}
	}

	if c.NatCount < -1 || c.NatCount > len(c.AvailabilityZones) {
		errs = multierror.Append(errs, fmt.Errorf("nat_count can be -1, 0, or <= nb of azs"))
	}

	return errs.ErrorOrNil()
}

// ParseConfig reads the configuration file at the given path and returns a new
// Config struct with the data populated.
func ParseConfig(parsed map[string]interface{}) (*Config, error) {
	var errs *multierror.Error

	utils.FlattenKeys(parsed)

	// Create a new, empty config
	config := new(Config)

	// Use mapstructure to populate the basic config fields
	metadata := new(mapstructure.Metadata)
	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		DecodeHook: mapstructure.ComposeDecodeHookFunc(
			mapstructure.StringToSliceHookFunc(","),
			mapstructure.StringToTimeDurationHookFunc(),
		),
		ErrorUnused: true,
		Metadata:    metadata,
		Result:      config,
	})

	if err != nil {
		errs = multierror.Append(errs, err)
		return nil, errs.ErrorOrNil()
	}
	if err := decoder.Decode(parsed); err != nil {
		errs = multierror.Append(errs, err)
		return nil, errs.ErrorOrNil()
	}

	// Update the list of set keys
	if config.setKeys == nil {
		config.setKeys = make(map[string]struct{})
	}

	for _, key := range metadata.Keys {
		if _, ok := config.setKeys[key]; !ok {
			config.setKeys[key] = struct{}{}
		}
	}

	d := DefaultConfig()
	d.Merge(config)
	config = d

	return config, errs.ErrorOrNil()
}

// DefaultConfig returns the default configuration struct.
func DefaultConfig() *Config {
	config := &Config{
		TypeName:            providerTypeName,
		Region:              defaultRegion,
		Network:             defaultNetwork,
		NatCount:            defaultNatCount,
		DefaultInstanceType: defaultInstanceType,
		setKeys:             make(map[string]struct{}),
	}

	return config
}
