package ssh

import (
	"fmt"
	"github.com/hashicorp/go-multierror"
	"log"
)

// defaultTerraformKeypair is the default keypair which will be used as by terraform
// on the servers
const defaultTerraformKeypair = "terraform"

// Copy returns a deep copy of the current configuration. This is useful because
// the nested data structures may be shared.
func (c *Config) Copy() *Config {
	config := &Config{
		AuthorizedKeys: c.AuthorizedKeys,
	}
	if c.Keypairs != nil {
		config.Keypairs = make(map[string]*KeypairConfig)
		for id, keypair := range c.Keypairs {
			config.Keypairs[id] = keypair
		}
	}
	return config
}

func (c *Config) Merge(config *Config, setKeys map[string]struct{}) {

	if WasSet(setKeys, "ssh.keypair") {
		if c.Keypairs == nil {
			c.Keypairs = make(map[string]*KeypairConfig)
		}
		for id, keypair := range config.Keypairs {
			c.Keypairs[id] = keypair
		}
	} else {
		c.Keypairs[defaultTerraformKeypair] = &KeypairConfig{
			Generated: true,
		}
	}

	if WasSet(setKeys, "ssh.authorized_keys") {
		c.AuthorizedKeys = config.AuthorizedKeys
	} else {
		c.AuthorizedKeys = []string{defaultTerraformKeypair}
	}
}

// WasSet determines if the given key was set in the config (as opposed to just
// having the default value).
func WasSet(setKeys map[string]struct{}, key string) bool {
	if _, ok := setKeys[key]; ok {
		return true
	}
	return false
}

// Config is the configuration for ssh
type Config struct {
	// keypair list of keypairs to configure in the project
	Keypairs map[string]*KeypairConfig `mapstructure:"keypair"`

	// authorized_keys: list of public ssh keys that will
	// be uploaded on servers as authorized_keys
	// keybase ids can also be used in the form of "keybase:id"
	AuthorizedKeys []string `mapstructure:"authorized_keys"`
}

// String is the string representation of this atlas configuration.
func (c *Config) String() string {
	return fmt.Sprintf(
		"{keypairs: %v, authorized_keys: %v}",
		c.Keypairs,
		c.AuthorizedKeys,
	)
}
func (c *Config) Validate() error {
	var errs *multierror.Error
	if c.Keypairs == nil || len(c.Keypairs) < 1 {
		errs = multierror.Append(errs, fmt.Errorf("ssh.keypairs. are required"))
	}
	for id, keypair := range c.Keypairs {
		if err := keypair.Validate(); err != nil {
			errs = multierror.Append(errs, fmt.Errorf("ssh.keypair.%s: %v", id, err))
		}
	}

	return errs.ErrorOrNil()
}

// KeypairConfig is the configuration for an ssh keypair
type KeypairConfig struct {
	// public_key - the public ssh key used for keypairs
	PublicKey string `mapstructure:"public_key"`

	// generated if set to true, a generated keypair will be used
	// by terraform
	Generated bool `mapstructure:"generated"`
}

// String is the string representation of this ssh configuration.
func (c *KeypairConfig) String() string {
	return fmt.Sprintf(
		"{public_key: %s, generated: %v}",
		c.PublicKey,
		c.Generated,
	)
}
func (c *KeypairConfig) Validate() error {
	var errs *multierror.Error
	if c.PublicKey == "" && c.Generated == false {
		errs = multierror.Append(errs, fmt.Errorf("ssh.public_key is required"))
	}

	if c.PublicKey != "" && c.Generated == true {
		log.Println("[WARN] both public key and generated mode are set. will use user provided keypair.")
	}

	return errs.ErrorOrNil()
}

func DefaultConfig() *Config {
	config := &Config{
		Keypairs: map[string]*KeypairConfig{},
	}
	return config
}
