package vault

import (
	"fmt"
	"github.com/hashicorp/go-multierror"
	"log"
	"strings"
)

// defaultServersCount is the default number of vault servers to bootstrap
const defaultServersCount = 3

// defaultKeyThreshold is the default key threshold used by vault
const defaultKeyThreshold = 3

// defaultSslBackendGeneratedRootCA is the default value for the root ca cert
// used by vault generation mode. Setting to true will generate a self
// signed certificate
const defaultSslBackendGeneratedRootCA = true

// Copy returns a deep copy of the current configuration. This is useful because
// the nested data structures may be shared.
func (c *Config) Copy() *Config {
	config := &Config{
		ServersCount: c.ServersCount,
		KeyThreshold: c.KeyThreshold,
	}
	config.PgpKeys = make([]string, len(c.PgpKeys))
	copy(c.PgpKeys, config.PgpKeys)
	if c.SslBackend != nil {
		config.SslBackend = &SslBackendConfig{}
		if c.SslBackend.RootCA != nil {
			config.SslBackend.RootCA = &SslBackendRootCAConfig{
				Cert:      c.SslBackend.RootCA.Cert,
				Key:       c.SslBackend.RootCA.Key,
				Generated: c.SslBackend.RootCA.Generated,
			}
		}
	}

	return config
}

func (c *Config) Merge(config *Config, setKeys map[string]struct{}) {
	if WasSet(setKeys, "vault.servers_count") {
		c.ServersCount = config.ServersCount
	}
	if WasSet(setKeys, "vault.pgp_keys") {
		c.PgpKeys = config.PgpKeys
	}
	if WasSet(setKeys, "vault.key_threshold") {
		c.KeyThreshold = config.KeyThreshold
	}
	if WasSet(setKeys, "vault.ssl_backend") {
		if c.SslBackend == nil {
			c.SslBackend = &SslBackendConfig{}
		}
		if WasSet(setKeys, "vault.ssl.root_ca") {
			if c.SslBackend.RootCA == nil {
				c.SslBackend.RootCA = &SslBackendRootCAConfig{}
			}
			if WasSet(setKeys, "vault.ssl.root_ca.cert") {
				c.SslBackend.RootCA.Cert = config.SslBackend.RootCA.Cert
			}
			if WasSet(setKeys, "vault.ssl.root_ca.key") {
				c.SslBackend.RootCA.Key = config.SslBackend.RootCA.Key
			}
			if WasSet(setKeys, "vault.ssl.root_ca.generated") {
				c.SslBackend.RootCA.Generated = config.SslBackend.RootCA.Generated
			}
		}
	}
}

// WasSet determines if the given key was set in the config (as opposed to just
// having the default value).
func WasSet(setKeys map[string]struct{}, key string) bool {
	if _, ok := setKeys[key]; ok {
		return true
	}
	return false
}

// Config is the configuration for vault
type Config struct {
	// servers_count - number of vault servers
	ServersCount int `mapstructure:"servers_count"`

	// key_threshold: threshold for vault unseal keys
	// cannot be equal to 1 if there's more than one key.
	// cannot be superior to the number of keys
	// defaults to the number of keybase ids up to 3
	KeyThreshold int `mapstructure:"key_threshold"`

	// pgp_keys: list of pgp keys that will
	// be used by vault as input to generate & encrypt unseal keys
	// currently, only keybase ids in the form of "keybase:id" is supported
	// required
	PgpKeys []string `mapstructure:"pgp_keys"`

	// Ssl is the ssl configuration for vault.
	SslBackend *SslBackendConfig `mapstructure:"ssl_backend"`
}

// String is the string representation of this atlas configuration.
func (c *Config) String() string {
	return fmt.Sprintf(
		"servers_count: %d, key_threshold: %d, pgp_keys: %s, ssl: %s",
		c.ServersCount,
		c.KeyThreshold,
		c.PgpKeys,
		c.SslBackend,
	)
}
func (c *Config) Validate() error {
	var errs *multierror.Error

	if c.ServersCount < 0 {
		errs = multierror.Append(errs, fmt.Errorf("vault.servers_count must be positive"))
	}

	if c.PgpKeys == nil {
		errs = multierror.Append(errs, fmt.Errorf("vault.pgp_keys is required"))
	} else {
		for i, k := range c.PgpKeys {
			if !strings.HasPrefix(k, "keybase:") {
				errs = multierror.Append(errs, fmt.Errorf("vault.pgp_keys.%d: Currently only keybase ids in the form of \"keybase:id\" is supported", i))
			}
		}
	}

	if nbIds := len(c.PgpKeys); c.KeyThreshold == 0 {
		if nbIds > 3 {
			c.KeyThreshold = 3
		} else {
			c.KeyThreshold = nbIds
		}
	} else {
		if c.KeyThreshold > nbIds {
			errs = multierror.Append(errs, fmt.Errorf("vault.key_threshold cannot be superior to the number of vault.pgp_keys"))
		}
	}

	return errs.ErrorOrNil()
}

// SslBackend is the configuration for vaultSslBackend
type SslBackendConfig struct {
	// root_cert - The cert of the Root CA TLS certificate
	// vault will use to mount its pki backend
	RootCA *SslBackendRootCAConfig `mapstructure:"root_ca"`
}

// String is the string representation of this vaultSslBackend configuration.
func (c *SslBackendConfig) String() string {
	return fmt.Sprintf(
		"root_ca: %v",
		c.RootCA,
	)
}
func (c *SslBackendConfig) Validate() error {
	var errs *multierror.Error

	if err := c.RootCA.Validate(); err != nil {
		errs = multierror.Append(errs, err)
	}

	return errs.ErrorOrNil()
}

// SslBackend is the configuration for vaultSslBackend
type SslBackendRootCAConfig struct {
	// cert - The cert of the Root CA TLS certificate
	// vault will use to mount its pki backend
	Cert string `mapstructure:"cert"`

	// key - The private key of the Root CA TLS certificate
	// vault will use to mount its pki backend
	Key string `mapstructure:"key"`

	// generated if set to true, a generated self signed certificate will be used
	// by vault as the root ca
	Generated bool `mapstructure:"generated"`
}

// String is the string representation of this vaultSslBackend configuration.
func (c *SslBackendRootCAConfig) String() string {
	return fmt.Sprintf(
		"cert: %s, key: %s, generated: %v",
		c.Cert,
		c.Key,
		c.Generated,
	)
}
func (c *SslBackendRootCAConfig) Validate() error {
	var errs *multierror.Error
	if c.Cert == "" && c.Generated == false {
		errs = multierror.Append(errs, fmt.Errorf("vault.ssl.cert is required"))
	}

	if c.Key == "" && c.Generated == false {
		errs = multierror.Append(errs, fmt.Errorf("vault.ssl.key is required"))
	}

	if c.Cert != "" && c.Generated == true {
		log.Println("[WARN] both root cert and generated mode are set. will use user provided certificate.")
	}

	return errs.ErrorOrNil()
}

func DefaultConfig() *Config {
	config := &Config{
		ServersCount: defaultServersCount,
		KeyThreshold: defaultKeyThreshold,
		SslBackend: &SslBackendConfig{
			RootCA: &SslBackendRootCAConfig{
				Generated: defaultSslBackendGeneratedRootCA,
			},
		},
	}
	return config
}
