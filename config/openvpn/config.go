package openvpn

import (
	"fmt"
	"github.com/hashicorp/go-multierror"
	"net"
)

// defaultClientNetwork is the default network that will be used
// by openvpn clients
const defaultClientNetwork = "172.31.31.0/24"

// Copy returns a deep copy of the current configuration. This is useful because
// the nested data structures may be shared.
func (c *Config) Copy() *Config {
	return &Config{
		ClientNetwork: c.ClientNetwork,
	}
}

func (c *Config) Merge(config *Config, setKeys map[string]struct{}) {
	if WasSet(setKeys, "openvpn.client_network") {
		c.ClientNetwork = config.ClientNetwork
	}
}

// WasSet determines if the given key was set in the config (as opposed to just
// having the default value).
func WasSet(setKeys map[string]struct{}, key string) bool {
	if _, ok := setKeys[key]; ok {
		return true
	}
	return false
}

// Atlas is the configuration for atlas
type Config struct {
	// client_network - this network will be the route pushed to the
	// vpn clients
	// required defaults to 172.31.31.0/24
	ClientNetwork string `mapstructure:"client_network"`
}

// String is the string representation of this atlas configuration.
func (c *Config) String() string {
	return fmt.Sprintf("client_network: %s", c.ClientNetwork)
}
func (c *Config) Validate() error {
	var errs *multierror.Error

	if c.ClientNetwork == "" {
		errs = multierror.Append(errs, fmt.Errorf("openvpn.client_network is required"))
	}

	if _, _, err := net.ParseCIDR(c.ClientNetwork); err != nil {
		errs = multierror.Append(errs, fmt.Errorf("openvpn.client_network must be a valid IPv4 CIDR"))
	}
	return errs.ErrorOrNil()
}

func DefaultConfig() *Config {
	config := &Config{
		ClientNetwork: defaultClientNetwork,
	}
	return config
}
