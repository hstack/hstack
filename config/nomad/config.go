package nomad

import (
	"fmt"
	"github.com/hashicorp/go-multierror"
)

// defaultServersCount is the default number of nomad servers to bootstrap
const defaultServersCount = 3

// Copy returns a deep copy of the current configuration. This is useful because
// the nested data structures may be shared.
func (c *Config) Copy() *Config {
	return &Config{
		ServersCount: c.ServersCount,
	}
}

func (c *Config) Merge(config *Config, setKeys map[string]struct{}) {
	if WasSet(setKeys, "nomad.servers_count") {
		c.ServersCount = config.ServersCount
	}
}

// WasSet determines if the given key was set in the config (as opposed to just
// having the default value).
func WasSet(setKeys map[string]struct{}, key string) bool {
	if _, ok := setKeys[key]; ok {
		return true
	}
	return false
}

// Nomad is the configuration for nomad
type Config struct {
	// servers_count - number of nomad servers
	ServersCount int `mapstructure:"servers_count"`
}

// String is the string representation of this atlas configuration.
func (c *Config) String() string {
	return fmt.Sprintf(
		"servers_count: %d",
		c.ServersCount,
	)
}
func (c *Config) Validate() error {
	var errs *multierror.Error
	if c.ServersCount < 0 {
		errs = multierror.Append(errs, fmt.Errorf("nomad.servers_count must be positive"))
	}
	return errs.ErrorOrNil()
}

func DefaultConfig() *Config {
	config := &Config{
		ServersCount: defaultServersCount,
	}
	return config
}
